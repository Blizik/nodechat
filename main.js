const blessed = require('blessed')
const commandLineArgs = require('command-line-args')
const fs = require('fs-extra')

const { Command } = require('./command')
const { Message } = require('./message')

const optionDefinitons = [
    { name: 'ip', type: String, defaultOption: true },
    { name: 'server', type: Boolean },
    { name: 'port', alias: 'p', type: Number },
    { name: 'max-clients', alias: 'm', type: Number}
]

let err_list = ''
let userSettings = {}
let options = commandLineArgs(optionDefinitons)

// Handle insufficient arguments and set default arguments
if (!options.ip && !options.server) {
    console.log('No ip found to connect to! Closing in 5 seconds...')

    console.log(options)

    let t = Date.now() + 5000
    while (Date.now() < t) {}
    process.exit(0)
}
if (options.ip && options.server)
    err_list += 'Unnecessary to use IP argument if starting as server. Ignoring.\n'
if (!options.port)
    options.port = 42069 // Dank ass default port
if (!options['max-clients'])
    options['max-clients'] = 10
if (options.ip && !options.server)
    options.ip = 'ws://' + options.ip + ':' + options.port

// Read user settings
try {
    userSettings = fs.readJSONSync('./settings.json')
    userSettings.name = userSettings.name.replace('\\', '\\\\')
} catch (err) {
    err_list += err + '\n'
    userSettings.name = 'User-' + Date.now()
}

// Initialize screen elements
let screen = blessed.screen()
let body = blessed.box({
    top: 1,
    left: 'center',
    height: '100%-3',
    width: '100%-4',
    tags: true,
    mouse: true,
    alwaysScroll: true,
    scrollable: true
})
let inputPos = blessed.box({
    content: '>',
    bottom: 0,
    left: 0,
    height: 1,
    width: 1,
    keys: false,
    style: {
        fg: 'white'
    }
})
let inputBar = blessed.textbox({
    bottom: 0,
    left: 2,
    height: 1,
    width: '100%-2',
    keys: true,
    mouse: true,
    inputOnFocus: true,
    style: {
        fg: 'white'
    }
})

// Add body to blessed screen
screen.append(body)
screen.append(inputBar)
screen.append(inputPos)

// Close the example on Escape -- only for development
screen.key(['escape'], (ch, key) => process.emit('SIGHUP'))
// screen.key(['escape'], (ch, key) => inputBar.focus())

// Add text to body
function log(msgObject) {
    if (body.content)
        body.content += '\n'
    if (msgObject.type === 'msg')
        body.content += '{green-fg}<' + msgObject.name + '>:{/} ' + msgObject.msg
    else if (msgObject.type === 'connect')
        body.content += msgObject.name + ' connected!'
    else if (msgObject.type === 'disconnect')
        body.content += msgObject.name + ' disconnected!'
    else if (msgObject) // Not actually message object, just text
        body.content += msgObject + '{/}'

    body.setScrollPerc(100)

    screen.render()
    
    if (!inputBar.focused)
        inputBar.focus()
}

// Idea here is to optionally start a server, but always start a client
// If you start a server, it'll handle the local client exactly the same as a remote one
if (options.server) {
    options.ip = 'ws://localhost:' + options.port
    const { Server } = require('./server')
    ChatServer = new Server({ port: options.port })

    log('Server started on port ' + options.port)

    let numClients = 0
    
    ChatServer.on('connection', ws => {
        if (numClients + 1 > options['max-clients']) {
            ws.close()
            return
        }

        numClients += 1

        ws.on('message', msgString => {
            ChatServer.recvMsg(msgString, ws)
        })

        ws.on('close', () => {
            numClients -= 1
        })

        ws.on('error', err => {})
    })

    ChatServer.on('error', err => {})
}

const { Client } = require('./client')
ChatClient = new Client(options.ip)

ChatClient.on('open', () => {
    let msgToSend = new Message('', userSettings.name, 'connect')
    ChatClient.send(JSON.stringify(msgToSend))
})

ChatClient.on('close', () => {
    process.emit('SIGHUP')
})

ChatClient.on('message', msgString => {
    let msgObject = JSON.parse(msgString)
    log(msgObject)
})

ChatClient.on('error', err => {})

// Handle submitting data
inputBar.on('submit', text => {
    inputBar.clearValue()

    if (text) {
        let args = text.split(' ')
        args[0] = args[0].toLowerCase()

        let cmd = Command.getCommand(args[0])

        if (cmd) {
            if (cmd.sfnc) {
                let msgToSend = new Message(text, userSettings.name, 'cmd')
                ChatClient.send(JSON.stringify(msgToSend))
            }
            cmd.fire(args)
        } else {
            let msgToSend = new Message(text, userSettings.name, 'msg')
            log(msgToSend)
            ChatClient.send(JSON.stringify(msgToSend))
        }
    }

    if (!inputBar.focused)
        inputBar.focus()
})

log(err_list)
screen.render()

// Commands (args is list of words in the string sent as a command INCLUDING THE COMMAND ITSELF AS args[0])
new Command(['/name', '/nick'], 1, (args, err) => {
    if (err) {
        log(err)
    } else {
        userSettings.name = args[1]
    }
}, (server, msgObject, err) => {
    if (err) {
        // ???
    } else {
        let args = msgObject.msg.split(' ')
        let newName = args[1]
        let msgToSend = msgObject.name + ' is now known as ' + newName
        server.broadcast(msgToSend)
    }
})

// Handle close event (when user clicks 'x')
process.on('SIGHUP', () => {
    let msgToSend = new Message('', userSettings.name, 'disconnect')
    ChatClient.send(JSON.stringify(msgToSend))

    fs.ensureFile('./settings.json').then(() => {
        fs.writeJSON('./settings.json', userSettings, {spaces: '\t'}).then(() => {

            process.exit(0)
        })
    })
})