const ws = require('ws')
const wss = ws.Server

const { Command } = require('./command')

class Server extends wss {
    recvMsg(msgString, ws) {
        let msgObject = JSON.parse(msgString)

        switch (msgObject.type) {
            case 'connect':
                this.broadcast(msgObject)
                break;
            case 'disconnect':
                this.broadcast(msgObject)
                break;
            case 'msg':
                this.broadcast(msgObject, ws)
                break;
            case 'cmd':
                this.cmd(msgObject, ws)
                break;
        }
    }

    broadcast(msgObject, from) {
        this.clients.forEach(client => {
            if (client.readyState === ws.OPEN && client !== from) {
                client.send(JSON.stringify(msgObject))
            }
        })
    }

    broadcastString(msgString, from) {
        this.clients.forEach(client => {
            if (client.readyState === ws.OPEN && client !== from) {
                client.send(JSON.stringify(msgString))
            }
        })
    }

    cmd(msgObject, ws) {
        let args = msgObject.msg.split(' ')
        let cmdToFire = Command.getCommand(args[0])
        if (cmdToFire.sfnc)
            cmdToFire.sfire(this, msgObject)
    }
}

module.exports = { Server }