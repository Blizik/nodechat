class Message {
    constructor(msg, name, type) {
        this.msg = msg
        this.name = name
        this.type = type
        this.ts = Date.now()
    }
}

module.exports = { Message }