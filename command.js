// No way to have static variable with 'class' definition of class
let cmdList = {}

class Command {
    constructor(aliases, args, fnc, sfnc) {
        this.aliases = aliases
        this.args = args
        this.fnc = fnc
        this.sfnc = sfnc    // Neglect this arg if there's no server component
        
        Command.addCommand(this)
    }

    // STATIC

    static addCommand(cmd) {
        if (cmd.aliases.every(x => !cmdList[x])) {
            cmd.aliases.forEach(x => { cmdList[x] = cmd })
        }
    }

    static getCommand(alias) {
        return cmdList[alias]
    }

    static list() {
        return cmdList
    }

    // PUBLIC

    addAlias(alias) {
        if (!cmdList[alias]) {
            this.aliases.push(alias)
            cmdList[alias] = this
        }
    }

    fire(args) { // args is list of all words !!INCLUDING THE COMMAND ITSELF AS args[0]!!
        if ((args.length > this.args)) { // && together anything else you need to check
            this.fnc(args)
        } else if (!(args.length > this.args)) {
            this.fnc(args, 'Requires additional arguments.')
        } // Add additional else if (! (...)) to catch and pass errors 
    }

    sfire(server, msgObject) {
        let args = msgObject.msg.split(' ')
        this.sfnc(server, msgObject)
    }
}

module.exports = { Command }